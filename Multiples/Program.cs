﻿using System;

namespace Multiples
{
    class Program
    {
        static void Main(string[] args)
        {
            var util = new Util();
            Console.WriteLine(util.FindMultiples(10000));
            Console.ReadKey();
        }
    }
}