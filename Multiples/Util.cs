﻿namespace Multiples
{
    public class Util
    {
        public int FindMultiples(int number)
        {
            int sum = 0;
            for(int i = 0; i < number; i++)
            {
                if(i % 5 == 0 || i % 3 == 0)
                {
                    sum += i;
                }
            }

            return sum;
        }
    }
}
