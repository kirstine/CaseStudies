﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Multiples.Tests
{
    [TestClass]
    public class UtilTests
    {
        private Util _util;

        [TestInitialize]
        public void Setup()
        {
            _util = new Util();
        }

        [TestMethod]
        public void Find_multiples_with_parameter_below_zero_should_return_sum_zero()
        {
            int expectedSum = 0;
            Assert.AreEqual(expectedSum, _util.FindMultiples(-6));
        }

        [TestMethod]
        public void Find_multiples_with_parameter_zero_should_return_sum_zero()
        {
            var expectedSum = 0;
            Assert.AreEqual(expectedSum, _util.FindMultiples(0));
        }

        [TestMethod]
        public void Find_multiples_with_parameter_ten_should_return_sum_twentythree()
        {
            var expectedSum = 23;
            Assert.AreEqual(expectedSum, _util.FindMultiples(10));
        }

        [TestMethod]
        public void Find_multiples_with_parameter_thousand_should_return_sum_233168()
        {
            var expectedSum = 233168;
            Assert.AreEqual(expectedSum, _util.FindMultiples(1000));
        }

    }
}
